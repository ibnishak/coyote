module coyote

go 1.14

require (
	github.com/ericaro/frontmatter v0.0.0-20200210094738-46863cd917e2
	github.com/gorilla/mux v1.7.4
	github.com/mitchellh/go-homedir v1.1.0
	github.com/spf13/cobra v1.0.0
	github.com/spf13/viper v1.7.0
	github.com/yuin/goldmark v1.1.32
	github.com/yuin/goldmark-meta v0.0.0-20191126180153-f0638e958b60
)
