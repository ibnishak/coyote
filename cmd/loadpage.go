package cmd

import (
	"bytes"
	"fmt"
	"html/template"
	"io/ioutil"
	"log"
	"time"

	"github.com/yuin/goldmark"
	meta "github.com/yuin/goldmark-meta"
	"github.com/yuin/goldmark/extension"
	"github.com/yuin/goldmark/parser"
	"github.com/yuin/goldmark/renderer/html"
)

func loadPage(title string) (*Page, error) {
	filename := title + ".md"
	body, err := ioutil.ReadFile(filename)
	if err != nil {
		log.Println(err)
	}
	markdown := goldmark.New(
		goldmark.WithExtensions(
			extension.Table,
			extension.Linkify,
			extension.TaskList,
			extension.DefinitionList,
			meta.Meta,
		),
		goldmark.WithParserOptions(
			parser.WithAutoHeadingID(),
		),
		goldmark.WithRendererOptions(
			html.WithHardWraps(),
			html.WithUnsafe(),
			html.WithXHTML(),
		),
	)
	var buf bytes.Buffer
	context := parser.NewContext()
	if err := markdown.Convert(body, &buf, parser.WithContext(context)); err != nil {
		panic(err)
	}
	metaData := meta.Get(context)
	bodyhtml := template.HTML(buf.String())
	const (
		layoutISO = "2006-01-02 15:04:05 -0700"
		layoutUS  = "Mon January 2 2006, 15:04 PM "
	)

	t, _ := time.Parse(layoutISO, fmt.Sprintf("%v", metaData["date"]))
	tags, ok := metaData["tags"].([]interface{})
	if !ok {
		fmt.Println("Tags not found in meta data or is not a slice")
	}
	s := make([]string, len(tags))
	for i, v := range tags {
		s[i] = fmt.Sprint(v)
	}
	return &Page{Slug: title, Title: fmt.Sprintf("%v", metaData["title"]), Tags: s, Body: bodyhtml, Date: t.Format(layoutUS)}, nil
}
