package cmd

import (
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"

	"github.com/ericaro/frontmatter"
	"github.com/gorilla/mux"
)

//Frontmatter is good
type Frontmatter struct {
	Title   string   `yaml:"title"`
	Tags    []string `yaml:"tags,flow"`
	Date    string   `yaml:"date"`
	Content string   `fm:"content" yaml:"-"`
}

func addtag(w http.ResponseWriter, r *http.Request) {
	//curl "http://127.0.0.1:2766/wiki/tags/add/apple?tag=new"
	title := mux.Vars(r)["title"]
	file := title + ".md"
	tag := r.FormValue("tag")

	err := actuallyaddtag(file, tag)
	if err != nil {
		fmt.Fprintf(w, err.Error())
		return
	}
	fmt.Fprintf(w, "Added tags")
}

func actuallyaddtag(file, tag string) error {
	v, err := readfile(file)
	if err != nil {
		return err
	}
	v.Tags = append(v.Tags, tag)

	data, err := frontmatter.Marshal(v)
	if err != nil {
		return err
	}
	err = ioutil.WriteFile(file, data, 0644)
	if err != nil {
		return err
	}
	tagfile := tag + ".md"
	f, err := os.OpenFile(tagfile, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
		log.Println("Error while opening tag file", err.Error())
	}
	defer f.Close()
	title := filenameWithoutExtension(file)
	tagline := fmt.Sprintf("- [%s](%s)\n", v.Title, title)
	if _, err := f.WriteString(tagline); err != nil {
		log.Println("Error while writing to tag file", err.Error())
	}
	return nil
}
