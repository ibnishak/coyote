package cmd

import (
	"fmt"
	"log"
	"net/http"
	"os"
	"os/exec"

	"github.com/gorilla/mux"
	"github.com/spf13/cobra"
)

var indexCmd = &cobra.Command{
	Use:   "index",
	Short: "Index the files given as argument",
	Long:  `Sanitise the title and rename the file. Add to tag files`,
	Run: func(cmd *cobra.Command, args []string) {
		for _, file := range args {
			err := index(file)
			if err != nil {
				fmt.Println(err.Error())
			}
		}
	},
}

func init() {
	rootCmd.AddCommand(indexCmd)
}

func indexHandler(w http.ResponseWriter, r *http.Request) {
	//curl "http://127.0.0.1:2766/wiki/index/{title}
	title := mux.Vars(r)["title"]
	file := title + ".md"
	err := index(file)
	if err != nil {
		fmt.Fprintf(w, err.Error())
		return
	}
	fmt.Fprintf(w, "Indexed file")
}

func index(file string) error {
	v, err := readfile(file)
	if err != nil {
		return err
	}
	if v.Title == "" {
		log.Println("No title")
		return nil
	}
	newlink := sanitise(v.Title)
	filename := newlink + ".md"
	if file != filename {
		_, err := os.Stat(filename)

		if err != nil {
			if os.IsNotExist(err) {
				os.Rename(file, filename)
				from := fmt.Sprintf("](%s)", filenameWithoutExtension(file))
				to := fmt.Sprintf("](%s)", newlink)
				cmd := exec.Command("fastmod", "-F", from, to, "--accept-all")
				if err := cmd.Run(); err != nil {
					fmt.Println("Error:", err)
				}
			}
		} else {
			log.Println("Error: File exists")
		}
	}
	for _, tag := range v.Tags {
		tagfile := tag + ".md"
		if !isStrExist(fmt.Sprintf("](%s)", newlink), tagfile) {
			tagline := fmt.Sprintf("- [%s](%s)\n", v.Title, newlink)
			f, err := os.OpenFile(tagfile, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
			if err != nil {
				log.Println(err)
			}
			defer f.Close()
			if _, err := f.WriteString(tagline); err != nil {
				log.Println(err)
			}
		}

	}
	return nil
}
