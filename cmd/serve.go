package cmd

import (
	"fmt"
	"html/template"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"os/exec"
	"path/filepath"

	"github.com/gorilla/mux"
	homedir "github.com/mitchellh/go-homedir"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

var (
	home, _   = homedir.Dir()
	templates = template.Must(template.ParseGlob(filepath.Join(home, ".config/coyote/templates/*.html")))
	port      string
)

// serveCmd represents the serve command
var serveCmd = &cobra.Command{
	Use:   "serve",
	Short: "Start the server",
	Long:  `Start the server at port from config`,
	Run: func(cmd *cobra.Command, args []string) {
		r := mux.NewRouter()
		r.HandleFunc("/{title}", viewHandler)
		r.HandleFunc("/edit/", editHandler)
		r.Path("/wiki/tags/add/{title}").Queries("tag", "{tag}").HandlerFunc(addtag)
		r.Path("/wiki/index/{title}").HandlerFunc(indexHandler)
		fmt.Println("Server starting at 127.0.0.1", port)
		log.Fatal(http.ListenAndServe(port, r))
	},
}

func init() {
	rootCmd.AddCommand(serveCmd)
	serveCmd.Flags().StringVarP(&port, "port", "p", viper.GetString("port"), "Port to serve on")
}

func viewHandler(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	title := vars["title"]
	log.Println("Loading ", title)
	p, err := loadPage(title)
	if err != nil {
		http.Redirect(w, r, "/edit/"+title, http.StatusFound)
		return
	}
	renderTemplate(w, "view", p)
}

func editHandler(w http.ResponseWriter, r *http.Request) {
	title := r.URL.Path[len("/edit/"):]
	fn := title + ".md"
	cmd := exec.Command("xdg-open", fn)
	cmd.Stdin = os.Stdin
	cmd.Stdout = os.Stdout
	err := cmd.Run()
	if err != nil {
		fmt.Println(err)
	}

}

func renderTemplate(w http.ResponseWriter, tmpl string, p *Page) {
	err := templates.ExecuteTemplate(w, tmpl+".html", p)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}

// Config is good
type Config struct {
	Tmpl string `yaml:"templates"`
	Port string `yaml:"port"`
}

//Page is good
type Page struct {
	Slug  string
	Title string
	Tags  []string
	Date  string
	Body  template.HTML
}

func (p *Page) save() error {
	filename := p.Title + ".md"
	return ioutil.WriteFile(filename, []byte(p.Body), 0600)
}
