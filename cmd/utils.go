package cmd

import (
	"io/ioutil"
	"path"
	"regexp"
	"strings"

	"github.com/ericaro/frontmatter"
)

func filenameWithoutExtension(fn string) string {
	return strings.TrimSuffix(fn, path.Ext(fn))
}

func readfile(file string) (*Frontmatter, error) {
	v := new(Frontmatter)
	body, err := ioutil.ReadFile(file)
	if err != nil {
		return v, err
	}

	err = frontmatter.Unmarshal(body, v)
	if err != nil {
		return v, err
	}
	return v, nil
}

func sanitise(str string) string {
	reg, err := regexp.Compile("[^A-Za-z0-9_]+")
	if err != nil {
		return ""
	}
	newStr := reg.ReplaceAllString(str, "-")
	newStr = strings.ToLower(newStr)
	return newStr
}

func isStrExist(str, filepath string) bool {
	b, err := ioutil.ReadFile(filepath)
	if err != nil {
		return false
	}

	isExist, err := regexp.Match(str, b)
	if err != nil {
		panic(err)
	}
	return isExist
}
